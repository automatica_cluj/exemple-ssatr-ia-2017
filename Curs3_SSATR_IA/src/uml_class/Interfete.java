package uml_class;

/**
 * Created by evo on 11/2/2017.
 */
public class Interfete {
    public static void main(String[] args) {
        //Play i1 = new Play();
        Play i2 = new Piano();
        i2.playNote("DO");

        Play i3 = new Piano();
        i3.playNote("RE");

        AudioDevice ad = new Piano();
        ad.sendSound();

        ((Play)ad).playNote("RE");

        ((Piano)ad).playNote("RE");

        Piano p3 = new Piano();

        p3.playNote("FA");
        p3.sendSound();


    }
}

class Piano implements Play, AudioDevice{

    @Override
    public void playNote(String n) {
        System.out.println("Piano play note "+n);
    }

    @Override
    public void sendSound() {
        System.out.println("Send note to audio device");
    }
}

interface AudioDevice {
    void sendSound();
}

interface Play{
    void playNote(String n);
}
