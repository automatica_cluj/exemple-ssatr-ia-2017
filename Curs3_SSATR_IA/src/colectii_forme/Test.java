package colectii_forme;

import javax.swing.*;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by evo on 11/2/2017.
 */
public class Test {
    public static void main(String[] args) {

        Drawing d = new Drawing();

        // create a basic JFrame
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("Drawing Example");
        frame.setSize(300,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // add panel to main frame
        frame.add(d);
        frame.setVisible(true);

        d.addShape(new Point(50,50));
        d.addShape(new Point(30,90));
        d.addShape(new Circle(100,200,90));


    }
}

class Drawing extends JPanel {

    ArrayList<Shape> shapes = new ArrayList<>();

    void addShape(Shape s){
        shapes.add(s);
    }

    public void paint(Graphics g){
//        for(Shape s: shapes)
//                s.draw(g);
        Iterator<Shape> i = shapes.iterator();
        while(i.hasNext())
            i.next().draw(g);
    }
}

abstract class Shape{
    abstract void draw(Graphics g);
}

class Point extends Shape{
    private int x;
    private int y;

    Point(){}

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    void draw(Graphics g) {
        System.out.println("Drawing point "+this);
        g.drawRect(x,y,10,10);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

class Circle extends Point{
    private int radius;

    Circle(int x, int y, int radius){
        super(x,y);
        this.radius = radius;
    }

    @Override
    void draw(Graphics g) {
        System.out.println("Drawing circle: "+this);
        g.drawOval(getX(),getY(),radius,radius);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}' + "with center: "+super.toString();
    }
}