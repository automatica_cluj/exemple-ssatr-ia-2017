package colectii_forme;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Created by evo on 11/2/2017.
 */
public class TestSortSet {
    public static void main(String[] args) {
        Person p1 = new Person("ABC", 12);
        Person p2 = new Person("XYZ", 32);
        Person p3 = new Person("PQR", 21);

        TreeSet<Person> mySet = new TreeSet<>();

        mySet.add(p1);
        mySet.add(p2);
        mySet.add(p3);

        for(Person p: mySet)
            System.out.println(p);

        System.out.println(".............");

        TreeSet<Person> mySet2 = new TreeSet<>( new PersonCompareByName());
        mySet2.add(p1);
        mySet2.add(p2);
        mySet2.add(p3);

        for(Person p: mySet)
            System.out.println(p);

        //immplementare sortare utilizand expresii lambda
        TreeSet<Person> mySet3 = new TreeSet<Person>( (x1,x2)->(x1.compareTo(x2)));
        mySet3.add(p1);
        mySet3.add(p2);
        mySet3.add(p3);

        mySet3.stream().forEach(System.out::println);
        mySet3.stream().forEach(d -> System.out.println(d));

        //exemplu java streams api
       long count2 =  mySet3.stream().map(p -> p.getAge()).filter(a -> a>=21).count();//.reduce(0,(a1,a2)->a1+a2);

        System.out.println("Number of users with age >= 21: "+count2);

    }
}


//utilizare comparator pentru implementare sortare alternativa
class PersonCompareByName implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return o1.compareTo(o2);
    }
}

//implementare comparable pentru implementare regula de sortare pentru tree set
class Person implements Comparable<Person>{
    int age;
    String name;

    public Person(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        return Integer.compare(this.age, o.age);
    }
}
