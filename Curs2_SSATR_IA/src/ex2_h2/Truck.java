
package ex2_h2;

public class Truck extends Car{
    
    private int mass;

    public Truck(int mass, String type, int cc, int speed) {
        super(type, cc, speed);
        this.mass = mass;
    }
    
    //overwrite
    void display(){
        System.out.println("Truck "+type+" "+cc+" "+speed+" "+mass);
    } 
    
}
