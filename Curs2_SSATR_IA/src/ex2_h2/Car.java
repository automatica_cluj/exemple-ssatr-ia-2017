
package ex2_h2;

class Car {
    public static final int MAX_SPEED = 220;
    protected String type;
    protected int cc;
    protected int speed;

    Car(String type, int cc, int speed) {
        this.type = type;
        this.cc = cc;
        this.speed = speed;
    }
    
    void acc(){
        if(speed<MAX_SPEED)
            speed++;
    }

    void dec(){
        if(speed>0)
            speed--;
    }
        
    int getSpeed() {
        return speed;
    }
    
    void display(){
        System.out.println("Car "+type+" "+cc+" "+speed);
    } 
   
}
