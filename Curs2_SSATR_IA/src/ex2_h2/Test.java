
package ex2_h2;

import java.util.Random;

public class Test {
   
   public static void main(String[] args){
           
       Car c1 = new Car("Dacia",1400,0);
       Car c2 = new Car("Opel",1950,0);
       Radar r1 = new Radar();
       r1.detectSpeed(c1);
       r1.detectSpeed(c2);       
       Car c3 = new Truck(100, "Scania", 7000, 0);
       Truck c4 = new Truck(200, "Iveco", 6000, 0);
       c3.display();
       c4.display();
       
   } 
}
