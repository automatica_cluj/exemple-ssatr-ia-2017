
package ex3_h3;

import ex2_h2.*;

public class Truck extends Car{
    
    private int mass;

    public Truck(int mass, String type, int cc, int speed) {
        super(type, cc, speed);
        this.mass = mass;
    }
    
    //overwrite
    void display(){
        System.out.println("Truck "+type+" "+cc+" "+speed+" "+mass);
    } 
    
    void displayMessageToDriver(String msg){
        System.out.println("Slow down:"+msg);
    }
    
    void load(String content){
        System.out.println("Loading truck with "+content); 
   }
    
}
