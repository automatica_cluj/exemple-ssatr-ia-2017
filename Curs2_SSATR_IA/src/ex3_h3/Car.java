
package ex3_h3;

import ex2_h2.*;

class Car {
    public static final int MAX_SPEED = 220;
    protected String type;
    protected int cc;
    protected int speed;
    protected Engine e;

    Car(String type, int cc, int speed) {
        this.type = type;
        this.cc = cc;
        this.speed = speed;
        e = new Engine();
    }
    
    void acc(){
        if(e.isStarted()==false)
            e.startEngine();
        if(speed<MAX_SPEED)
            speed++;
    }

    void dec(){
        if(speed>0)
            speed--;
    }
        
    int getSpeed() {
        return speed;
    }
    
    void displayMessageToDriver(String msg){
        System.out.println("Msg to driver:"+msg);
    }
        
    void display(){
        System.out.println("Car "+type+" "+cc+" "+speed);
    } 
   
}
