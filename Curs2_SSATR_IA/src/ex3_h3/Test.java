
package ex3_h3;

import ex2_h2.*;
import java.util.Random;

public class Test {
   
   public static void main(String[] args){
           
       Car c1 = new Car("Dacia",1400,0);
       Car c2 = new Car("Opel",1950,0);
  
       c1.display();
       c2.display();
       
       Random r = new Random();
       int v = r.nextInt(90);
       for(int i=1;i<v;i++)
           c1.acc();
       
       v = r.nextInt(90);
       for(int i=1;i<v;i++)
           c2.acc();
       
       Radar r1 = new Radar(); //instantiere 
       r1.detectSpeed(c1);
       r1.detectSpeed(c2);
       
       Car c3 = new Truck(100, "Scania", 7000, 0);
       Truck c4 = new Truck(200, "Iveco", 6000, 0);
       c3.display();
       c4.display();
       
       v = r.nextInt(90);
       for(int i=1;i<v;i++)
           c3.acc();
       
       v = r.nextInt(90);
       for(int i=1;i<v;i++)
           c4.acc();
       
       r1.detectSpeed(c3);
       r1.detectSpeed(c4);
       
       c4.load("bricks");
       
//       c3.load("apples"); //load nu este accesibila prin tipul de baza
        
// eroare la compilare      ((Dog)c3 ).load("apples"); //conversie de tip
       
       ((Truck)c3 ).load("apples"); //conversie de tip
       
       if(c1 instanceof Truck)
        ((Truck)c1).load("oranges"); //eroare la runtime
       else
           System.out.println("Cannot convert to truck.");
   } 
}
