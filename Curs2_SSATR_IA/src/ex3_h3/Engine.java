/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex3_h3;

/**
 *
 * @author evo
 */
public class Engine {
    boolean started;
    
    void startEngine(){
        started = true;
        System.out.println("ENGINE STARTED");
    }
    
    void stopEngine(){
        started = false;
    }
    
    boolean isStarted(){
        return started;
    }
}
