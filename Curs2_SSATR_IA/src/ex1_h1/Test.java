
package ex1_h1;

public class Test {
//varianta 1 care nu mai funcitoneaza dupa incapsularea atributelor deoarece atributele nu mai sunt accesibile in exterior
//   static void display(Car c){
//       if(c!=null)
//        System.out.println("Car "+c.type+" "+c.cc+" "+c.speed);
//   } 
   
   public static void main(String[] args){
       Car c1 = new Car("Dacia",1400,0);
       Car c2 = new Car("Opel",1950,0);
       Car c3 = null;
       
// varianta de initliazare fara constructor
//       c1.type = "Dacia";
//       c1.cc = 1400;
//       c1.speed = 0;
//       
//       c2.type = "Opel";
//       c2.cc = 1950;
//       c2.speed = 0;
       
//varianta 1 de afisare       
//       display(c1);
//       display(c2);
//       display(c3);
       
       
//varianat 2
       c1.display();
       c2.display();
       
       Radar r1 = new Radar(); //instantiere 
       r1.detectSpeed(c1);
       r1.detectSpeed(c2);
   } 
}
