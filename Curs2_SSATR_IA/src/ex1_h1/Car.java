
package ex1_h1;

class Car {
    private String type;
    private int cc;
    private int speed;

    Car(String type, int cc, int speed) {
        this.type = type;
        this.cc = cc;
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }
    
    void display(){
        System.out.println("Car "+type+" "+cc+" "+speed);
    } 
   
}
