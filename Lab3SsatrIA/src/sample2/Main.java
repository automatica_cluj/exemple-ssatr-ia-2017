package sample2;


class T1 extends Thread{
    int k;
    boolean done;

    @Override
    public void run() {
        synchronized (Main.LOCK) {
            for (int i = 0; i < 50; i++) {
                System.out.println("Counter = " + k + " " + getName());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                k++;
            }
            done = true;
            System.out.println("DONE ");
            Main.LOCK.notifyAll();
        }


        System.out.println("EXIT FROM RUN "+isDone());
    }

    public boolean isDone() {
         System.out.println("RETURN ISDONE :"+done);
        return done;
    }
}

class T2 extends Thread{
    T1 t;
    int k;

    public T2(T1 t) {
        this.t = t;
    }

    @Override
    public void run() {

//        if(!t.isDone()){
//            synchronized (Main.LOCK){
//                try {
//                    Main.LOCK.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        while(!t.isDone()){
            synchronized (Main.LOCK){
                try {
                    Main.LOCK.wait(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("PREPARE TO RUN");
        synchronized (Main.LOCK) {
            k = 101;
            for (int i = 101; i < 200; i++) {
                System.out.println("Counter = " + k + " " + getName());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                k++;
            }
        }
    }
}

public class Main {
    static Integer LOCK = new Integer(0);

    public static void main(String[] args) {
        System.out.println("Hello Threads!");
        T1 a = new T1();
        T2 b = new T2(a);
        a.start();
        b.start();
    }
}
