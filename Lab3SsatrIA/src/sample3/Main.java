package sample3;

import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

class T1 extends Thread{
    int k;
    boolean done;

    @Override
    public void run() {

            Main.LOCK_2.lock();

            for (int i = 0; i < 100; i++) {
                System.out.println("Counter = " + k + " " + getName());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                k++;
            }

            Main.LOCK_2.unlock();

            done = true;
            System.out.println("DONE ");
        }

    public boolean isDone() {
       // System.out.println("RETURN ISDONE :"+done);
        return done;
    }
}

class T2 extends Thread{
    T1 t;
    int k;

    public T2(T1 t) {
        this.t = t;
    }

    @Override
    public void run() {

        //try {
            //if(Main.LOCK_2.tryLock(100,TimeUnit.SECONDS)){
            if(t.isDone()){
                System.out.println("PREPARE TO RUN");
                k = 101;
                for (int i = 101; i < 200; i++) {
                    System.out.println("Counter = " + k + " " + getName());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    k++;
                }
                //Main.LOCK_2.unlock();
            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        while(!t.isDone()){
//            try {
//                Main.LOCK_2.tryLock(100,TimeUnit.MILLISECONDS);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }


    }
}

public class Main {
    static Integer LOCK = new Integer(0);
    static ReentrantLock LOCK_2 = new ReentrantLock();

    public static void main(String[] args) {
        System.out.println("Hello Threads!");
        T1 a = new T1();
        T2 b = new T2(a);
        a.start();
        b.start();
    }
}
