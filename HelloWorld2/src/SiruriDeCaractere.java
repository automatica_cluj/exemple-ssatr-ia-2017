/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author evo
 */
public class SiruriDeCaractere {

    public static void main(String[] args) {
        String s1 = "ABC";
        String s2 = new String("123");

        if (s1.equals(s2)) {
            System.out.println("Sirurile egale");
        } else {
            System.out.println("Sirurile diferite");
        }

        String s3 = s1 + s2;
        System.out.println(s3);

        String s4 = s2 + "xyz";
        System.out.println(s4);

        String s5 = s2 + 124;
        System.out.println(s4);
        
        if(s2.indexOf("23")!=-1){
            System.out.println("Subsirul 23 exista");
        }
        
        s4 = s4.toUpperCase();
        System.out.println(s4);

    }
}
