
import java.util.Scanner;

public class CitireDeLaTastatura {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        char c = 'd';
        while (c == 'd') {
            System.out.println("Introducet operand 1:");
            int op1 = s.nextInt();
            System.out.println("Introducet operand 2:");
            int op2 = s.nextInt();

            System.out.println("Introduceti operatia:");
            //+ - * / 
            char op = s.next(".").charAt(0);
            int rezultat = 0;
            switch (op) {
                case '+':
                    rezultat = op1 + op2;
                    break;
                case '-':
                    rezultat = op1 - op2;
                    break;

                default:
                    System.out.println("Operatia nu este suportata!");
            }

            System.out.println("Rezultatul este:" + rezultat);
            System.out.println("Doriti o alta operatie (d/n)? ");
            //c = s.next(".").charAt(0);
            s.nextLine();
            c = s.nextLine().charAt(0);
        }
    }

}
