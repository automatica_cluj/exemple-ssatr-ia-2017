import java.util.Arrays;
import java.util.Random;

public class ExempluSortare {
    public static void main(String[] args) {
        Random r = new Random();
        System.out.println(r.nextInt(100));
        int[] a = new int[10];
        
        for(int i=0;i<a.length;i++)
            a[i] = r.nextInt(100);
        
        for(int k:a)
            System.out.println(k);
        
        boolean c = true;
        while(c){
            c = false;
            for(int i=0;i<a.length-1;i++){
                if(a[i]<a[i+1]){
                    int t = a[i];
                    a[i] = a[i+1];
                    a[i+1] = t;
                    c = true;
                }
            }
        }

//        Arrays.sort(a);
        System.out.println("++++");
        for(int k:a)
            System.out.println(k);
        
        
    }
}
