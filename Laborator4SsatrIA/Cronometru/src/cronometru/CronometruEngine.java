
package cronometru;

import java.util.logging.Level;
import java.util.logging.Logger;


public class CronometruEngine extends Thread {
    int value;
    boolean active = true;
    UICronometru ui;

    public CronometruEngine(UICronometru ui) {
        this.ui = ui;
        ui.setEngine(this);
    }
    
    public void run(){
        while(true){
            try {
                if(active){
                 value++;
                 ui.setTime(""+value);
                 Thread.sleep(100);
                }else{
                    synchronized(ui){
                        ui.wait();
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(CronometruEngine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
   
    }//.run
   
    void pauseCronometru(){
        active = false;
    }
    
    synchronized void resumeCronometru(){
        active = true;
       // synchronized(ui){
            this.notify();
       // }
                
    }
}
