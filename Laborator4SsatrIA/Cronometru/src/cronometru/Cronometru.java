/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometru;

/**
 *
 * @author mihai.hulea
 */
public class Cronometru {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UICronometru ui = new UICronometru();
        CronometruEngine engine = new CronometruEngine(ui);
        ui.setVisible(true);
        engine.start();
    }
    
}
