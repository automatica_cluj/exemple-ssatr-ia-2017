
package demosim1;

public interface Model {
    public void step(int step);

    public boolean canStop();
    
}


