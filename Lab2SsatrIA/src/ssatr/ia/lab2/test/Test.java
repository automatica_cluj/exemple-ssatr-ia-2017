package ssatr.ia.lab2.test;

import java.util.Random;

/**
 * Created by evo on 11/6/2017.
 */
public class Test {

    public static void main(String[] args) {
        Point p1 = new Point(89,21);
        Random r = new Random();

        Point p2 =
                new Point(r.nextInt(100),r.nextInt(100));

        Point p3 = Point.GenerateRandomPoint();
        Point p4 = Point.GenerateRandomPoint();

        p1.draw();
        p2.draw();
        p3.draw();
        p4.draw();

        Point p5 = p1.GenerateRandomPoint();


    }
}
