package ssatr.ia.lab2.test;

public class Engine {
    String fuellType;
    long capacity;
    boolean active;
    int power;

    Engine(int capacity,boolean active){
        this.capacity = capacity;
        this.active = active;
    }
    Engine(int capacity,boolean active,String fuellType, int power){
        this(capacity,active);
        this.fuellType = fuellType;
        this.power = power;
    }
    Engine(){
        this(2000,false,"diesel",80);
    }
    void print(){
        System.out.println("Engine: capacity="+this.capacity+" fuell="+fuellType+" active="+active);
    }
    public static void main(String[] args) {
        Engine tdi = new Engine();
        Engine i16 = new Engine(1600,false,"petrol",110);
        Engine d30 = new Engine(3000,true,"diesel",200);
        tdi.print();i16.print();d30.print();
    }
}