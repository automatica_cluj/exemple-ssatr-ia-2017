package ssatr.ia.lab2.test;
import java.util.Random;

public class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void draw(){
        System.out.println("Point "+x+" "+y);
    }

    static Point GenerateRandomPoint(){
        Random r = new Random();
        return new Point(r.nextInt(100),r.nextInt(100));
    }


}
