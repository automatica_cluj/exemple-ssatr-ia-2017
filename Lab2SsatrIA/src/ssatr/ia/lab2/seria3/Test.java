package ssatr.ia.lab2.seria3;
import java.util.Random;

public class Test {

    static void accelerateVehicle(Vehicle x){
        Random r = new Random();
        for(int i=0;i<r.nextInt(300);i++)
            x.acc();
    }

    public static void main(String[] args) {

        Vehicle v1 = new Vehicle("BMW",0, "benzina");
        Vehicle v2 = new Vehicle("Opel",0, "electric");
        Vehicle v3 = new Vehicle("Dacia",0, "diesel");

        accelerateVehicle(v1);
        accelerateVehicle(v2);
        accelerateVehicle(v3);

        v1.displayVehicleDetails();
        v2.displayVehicleDetails();
        v3.displayVehicleDetails();
    }
}
