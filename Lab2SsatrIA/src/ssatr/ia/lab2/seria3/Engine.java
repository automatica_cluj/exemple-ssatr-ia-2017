package ssatr.ia.lab2.seria3;

class Engine {
    private String type;
    private boolean status;

    public Engine(String type){
        this.type = type;
        this.status = false;
    }

    boolean startStop(){
        status = !status;
        return status;
    }

    boolean isStarted(){
        return status;
    }

    public String getType() {
        return type;
    }
}
