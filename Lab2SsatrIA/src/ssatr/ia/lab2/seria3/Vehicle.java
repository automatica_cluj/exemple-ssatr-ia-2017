package ssatr.ia.lab2.seria3;

public class Vehicle {
    final static int MAX_SPEED = 220;
    private String name;
    private int speed;
    private Engine engine;

    public Vehicle(String name, int speed, String engineType) {
        this.name = name;
        this.speed = speed;
        engine = new Engine(engineType);
        engine.startStop();
    }

    void acc(){
        if(engine.isStarted()==false){
            System.out.println("Engine not started. Operation not permited!");
            return;
        }
        if(speed<=MAX_SPEED){
            speed++;
            displayVehicleDetails();
        }else{
            System.out.println("Cannot accelerate. Max speed reached.");
            displayVehicleDetails();
        }
    }

    void dec(){
        if(engine.isStarted()==false){
            System.out.println("Engine not started. Operation not permited!");
            return;
        }
        if(speed>0){
            speed--;
            displayVehicleDetails();
        }else{
            System.out.println("Cannot decelerate. Vehicle is stopped.");
            displayVehicleDetails();
        }
    }

    void displayVehicleDetails(){
        System.out.println("V "+name+" s "+speed+" e "+engine.getType());
    }
}
