package ssatr.ia.lab2;

public class Engine {
    String fuellType;
    long capacity;
    boolean active;
    int cilinders;

    Engine(int capacity,boolean active){
        this.capacity = capacity;
        this.active = active;
    }
    Engine(int capacity,boolean active,String fuellType, int cilinders){
        this(capacity,active);
        this.fuellType = fuellType;
        this.cilinders = cilinders;
    }
    Engine(){
        this(2000,false,"diesel", 4);
    }

    void print(){
        System.out.println("Engine: capacity="+this.capacity+" fuell="+fuellType+" active="+active);
    }
    public static void main(String[] args) {
        Engine tdi = new Engine(); // 3
        Engine electric = new Engine(1000,true); // 1
        Engine i16 = new Engine(1600,false,"petrol", 6); // 2
        Engine d30 = new Engine(3000,true,"diesel", 4); // 2

        tdi.print();i16.print();d30.print();
    }
}
