package ssatr.ia.lab2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by evo on 10/30/2017.
 */
public class TestDraw {
    public static void main(String[] args) {
        Draw d = new Draw();
        Point p1 = new Point(Color.red, 10, 20);
        Point p2 = new Point(Color.blue, 50, 40);
        d.addPoint(p1, 0);
        d.addPoint(p2, 1);

        // create a basic JFrame
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("JFrame Color Example");
        frame.setSize(300,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // add panel to main frame
        frame.add(d);
        frame.setVisible(true);

        Point p3 = new Point(Color.yellow, 150, 40);
        d.addPoint(p3, 3);
        d.drawAll();

        Circle c1 = new Circle(Color.CYAN, 70,90, 90);
        Circle c2 = new Circle(Color.CYAN, 170,90, 40);
        d.addPoint(c1,3);
        d.addPoint(c2,4);

        d.drawAll();
    }
}

class Draw extends JPanel {
    Point[] points = new Point[10];

    void addPoint(Point p, int i){
        points[i] = p;
    }

    void drawAll(){
        this.repaint();
    }

    public void paint(Graphics g) {
        for (int i=0;i<points.length;i++)
            if(points[i] != null) {
                points[i].draw(g);
            }
    }
}

class Circle extends Point{
    int radius;

    public Circle(Color color, int x, int y, int radius) {
        super(color, x, y);
        this.radius = radius;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(color);
        g.fillOval(x,y, radius, radius);
    }
}

class Point{
    Color color;
    int x;
    int y;

    public Point(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void draw(Graphics g){
        System.out.println("Point: "+color +" "+x+" "+y);
        g.setColor(color);
        g.fillRect(x, y, 10, 10); //draw a rectangle on x y position of width length 10

    }
}
