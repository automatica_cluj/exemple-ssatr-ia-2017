package ssatr.ia.lab2;
// https://pastebin.com/ZggLmKkz
public class Main {

    public static void main(String[] args) {

        Vehicle v1 = new Vehicle(0, false, "Audi A2");
        Vehicle v2 = new Vehicle(100,true, "Opel Insignia");
        Vehicle v3;
            v3 = new Vehicle(100,true, "Opel Insignia");

        v1.accelerate();
        v2.accelerate();

        v1.display();
        v2.display();
        v3 = v2;
        v3.accelerate();

    }
}

///////////////////////////////////////

class Vehicle{
    private int speed;
    private boolean started;
    private String type;

    public Vehicle(int speed, boolean started, String type) {
        this.speed = speed;
        this.started = started;
        this.type = type;
    }

    void accelerate(){
        if(started && speed < 220) {
            this.speed++;
            System.out.println("Current speed "+this.speed);
        }
        else
            System.out.println("Cannot increase speed!");
    }

    void display(){
        System.out.println("Vehicle "+type +" starte "+started+" speed "+speed);
    }

}

