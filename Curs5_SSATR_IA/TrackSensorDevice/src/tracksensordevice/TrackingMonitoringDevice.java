/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracksensordevice;

/**
 *
 * @author mihai.hulea
 */
public class TrackingMonitoringDevice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        SenzorTemperatura s1 = new SenzorTemperatura("ST-"+Math.random());
        System.out.println(s1);
        CititorSenzor cititor = new CititorSenzor(s1);
        cititor.start();
    }
    
}
