/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracksensordevice;

import java.util.Random;

/**
 *
 * @author mihai.hulea
 */
public class SenzorTemperatura extends Senzor{
    
    private int value;

    public SenzorTemperatura(int value, String name) {
        super(name);
        this.value = value;
    }

    public SenzorTemperatura(String name) {
        super(name);
    }
    
    public void readTemperature(){
        Random r = new Random();
        value = r.nextInt(100);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SenzorTemperatura{" + "value=" + value + " nume=" + getName()+'}';
    }
    
    
    
    
}
