/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tracksensordevice;

import java.io.BufferedOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mihai.hulea
 */
public class CititorSenzor extends Thread{
    SenzorTemperatura senzor;

    public CititorSenzor(SenzorTemperatura senzor) {
        this.senzor = senzor;
    }
    
    
    public void run(){
        try{
            Socket s = new Socket("127.0.0.1",7878); //conectare la server
            PrintWriter netOut = new PrintWriter(new BufferedOutputStream(s.getOutputStream()),true); //construire flux iesire
            while(true){
                senzor.readTemperature();
                System.out.println(senzor.getValue());
                netOut.println(senzor.getName()+":"+senzor.getValue());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CititorSenzor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
