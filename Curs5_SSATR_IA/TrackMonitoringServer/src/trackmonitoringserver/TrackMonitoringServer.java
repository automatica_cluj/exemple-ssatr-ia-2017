/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackmonitoringserver;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.StringTokenizer;

/**
 *
 * @author mihai.hulea
 */
public class TrackMonitoringServer {
    
    public static Connection getDbConnection() throws SQLException{
        Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/trackdata", "app", "app");
        System.out.println("Conecxiune la baza de date realizata!");
        return con;
    }
    
    public static void main(String[] args) throws IOException, SQLException {
        //realizare conexiune la baza de date si optinere obiect statement prin care timrit comenzile SQL
        Connection con = getDbConnection();
        Statement statement = con.createStatement();
        
        //initializare server si asteptare conexiuni de la client
        ServerSocket ss = new ServerSocket(7878);
        System.out.println("Astept conexiuni...");
        Socket s = ss.accept();
        
        //clientul s-a conectat. comunicare cu client
        System.out.println("Clientul s-a conectat!");
        //nume:valoare\n
        System.out.println(s.getRemoteSocketAddress());
        BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String line = in.readLine();
        while(line!=null){
            System.out.println("Receptionat de la client:"+line);            
            StringTokenizer st = new StringTokenizer(line,":");
            String name = st.nextToken();
            int value = Integer.parseInt(st.nextToken());
            statement.executeUpdate("INSERT INTO SENZOR_DATA (\"NAME\", \"VALUE\") VALUES ('"+name+"' , "+value+")");
            line = in.readLine();        
        }
        System.out.println("Conexiune finalizata.");
        
    }
    
}
