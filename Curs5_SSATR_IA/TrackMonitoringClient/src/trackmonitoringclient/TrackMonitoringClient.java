/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trackmonitoringclient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author mihai.hulea
 */
public class TrackMonitoringClient {
    
    public static String readAll(){
        try{
            Connection con = DriverManager.getConnection("jdbc:derby://localhost:1527/trackdata", "app", "app");
            System.out.println("Conecxiune la baza de date realizata!");
            Statement statement = con.createStatement();
            ResultSet result = 
                    statement.executeQuery("SELECT * FROM SENZOR_DATA");
            String r = "";
            while(result.next()){
                String name = result.getString("name");
                int value = result.getInt("value");
                 r = r + name+" "+value+" \n";
            }
            return r;
        }catch(Exception e){
            e.printStackTrace();
            return "Connection with database error.";
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       SensorDataJFrame window = new SensorDataJFrame();
    
       window.setVisible(true);
       
       
    }
    
}
