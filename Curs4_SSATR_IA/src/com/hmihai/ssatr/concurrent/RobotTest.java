package com.hmihai.ssatr.concurrent;
import java.util.Random;

public class RobotTest {
    public static void main(String[] args) {
        Coordinates coordinates = new Coordinates();
        GpsReader gps = new GpsReader(coordinates);
        Controller ctrl = new Controller(coordinates);
        gps.start();
        ctrl.start();
    }
}

class Controller extends Thread{
    Coordinates c;

    public Controller(Coordinates c) {
        this.c = c;
    }

    public void run(){
        while(true){
            synchronized (c) {
                int a1 = c.getLat();
                int a2 = c.getLon();
                System.out.println("Apply command for coordinates: [" + a1 + "," + a2 + "]");
            } //. end of synchronize
                try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class GpsReader extends Thread{
    Coordinates c;

    public GpsReader(Coordinates c) {
        this.c = c;
    }

    public void run(){
        Random r = new Random();

        while(true){
            int i = r.nextInt(100);
            int j = r.nextInt(100);
            System.out.println("WRITE ["+i+","+j+"]");
            synchronized(c) {
                c.setLat(i);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                c.setLon(j);
            }//. end synchronize

        }
    }
}

class Coordinates{
    private int lat;
    private int lon;

    public int getLat() {
       // System.out.println("READ lat"+lat);
        return lat;
    }

    public void setLat(int lat) {
        //System.out.println("WRITE lat"+lat);
        this.lat = lat;
    }

    public int getLon() {
       // System.out.println("READ lon"+lon);
        return lon;
    }

    public void setLon(int lon) {
        //System.out.println("WRITE lon"+lon);
        this.lon = lon;
    }
}
