package com.hmihai.ssatr.concurrent;

import java.util.Random;
public class Main {

    public static void main(String[] args) {
	    Vehicle v1 = new Vehicle("Audi");
        Vehicle v2 = new Vehicle("Renault");

        //v1.go(); //1.
        //v2.go(); //2.

        v1.start();
        v2.start();

        Vehicle1 a1 = new Vehicle1("BMW");
        Vehicle1 a2 = new Vehicle1("Opel");

        Thread t1 = new Thread(a1);
        t1.start();

        Thread t2= new Thread(a2);
        t2.start();


//        v1.run();
//        v2.run();
        //try{Thread.sleep(1000*1000);}catch(Exception e){}
    }
}

class Vehicle1 implements Runnable{
    private String name;

    public Vehicle1(String name) {
        this.name = name;
        //setDaemon(true);
        //setName(name);
    }

    public void run(){
        go();
    }

    void go(){
        Random r = new Random();
        for(int i=0;i<1000;i++){
            System.out.println("Vehicle "+name+" km "+i);

            //try{Thread.sleep(r.nextInt(500));}catch(Exception e){}
        }
    }
}

class Vehicle extends Thread{
    private String name;

    public Vehicle(String name) {
        this.name = name;
        //setDaemon(true);
        setName(name);
    }

    public void run(){
        go();
    }

    void go(){
        Random r = new Random();
        for(int i=0;i<10;i++){
            System.out.println("Vehicle "+name+" km "+i);

            try{Thread.sleep(r.nextInt(500));}catch(Exception e){}
        }
    }
}
