/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exempleui.mvc1temp;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author evo
 */
public class ThermometerController implements ActionListener{
    private Thermometer model;
    private ThermometerJFrame view;

    public ThermometerController(Thermometer model, ThermometerJFrame view) {
        this.model = model;
        this.view = view;
        
        view.registerChangeSignInterceptor(this);
        
        model.addObserver(view);
        view.setVisible(true);
        model.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        model.changeSign();
    }
    
    
}
