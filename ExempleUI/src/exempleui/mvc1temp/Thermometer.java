/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exempleui.mvc1temp;
import java.util.Observable;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author evo
 */
public class Thermometer extends Observable implements Runnable {
    
    boolean active = true;
    int temp;
    int sign = 1;
    
    public void start(){
        Thread t = new Thread(this);
        t.start();
    }
    
    public void run(){
        Random r = new Random();
        while(active){
            temp  = r.nextInt(100)*sign;
            System.out.println("Current temperature: "+temp);
            this.setChanged();
            this.notifyObservers(temp);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Thermometer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void changeSign(){
        sign = sign * -1;
    }

    public int getTemp() {
        return temp;
    }
    
    
    
}
