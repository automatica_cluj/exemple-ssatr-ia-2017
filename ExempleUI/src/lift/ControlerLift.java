/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lift;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author evo
 */
public class ControlerLift implements Observer{
    
    Lift lift;
    int etajDestinatie;
    LinkedList<Integer> queue = new LinkedList<>();

    public ControlerLift(Lift lift) {
        this.lift = lift;
        etajDestinatie = -1;
        lift.addObserver(this);
    }

    public void comandaEtaj(int etaj){
        if(etajDestinatie==-1){
            this.etajDestinatie = etaj;
            if(etajDestinatie*10>lift.getPosition()){
                lift.setDirection(1);         
            }else if(etajDestinatie*10<lift.getPosition()){
                lift.setDirection(-1);
            }
        }else{
            System.out.println("Liftul este ocupat."); 
            queue.addLast(etaj);
       }
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof Lift){
            Integer currentPostion = (Integer)(arg);
            if(etajDestinatie*10==currentPostion){
                lift.setDirection(0);
                System.out.println("DESHIDE USILE LA ETAJUL "+etajDestinatie);
                etajDestinatie = -1;
                
                if(queue.size()>0){
                    int c = queue.removeFirst();
                    comandaEtaj(c);
                }
            }
        }
    }
    
}
