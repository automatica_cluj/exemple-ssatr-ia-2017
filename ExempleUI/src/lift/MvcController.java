/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lift;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 *
 * @author evo
 */
public class MvcController implements ActionListener{
    Lift lift;
    LiftView view;
    ControlerLift ctrl;

    public MvcController(Lift lift, LiftView view, ControlerLift ctrl) {
        this.lift = lift;
        this.view = view;      
        this.ctrl = ctrl;
        
        view.registerListener(this);
        
        Thread t = new Thread(lift);
        t.start();
        lift.addObserver(view);
        view.setVisible(true);
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Integer etaj =
                Integer.parseInt(
                        ((JButton)e.getSource()).getText()
                );
        ctrl.comandaEtaj(etaj);
    }
    
    
    
}
