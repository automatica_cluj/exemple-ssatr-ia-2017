/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lift;

import java.util.Observable;

/**
 * 
 * 
 * 
 * 40
 * 30
 * 20
 * 10 
 * 0
 * @author evo
 */
public class Lift extends Observable implements Runnable{

   int dir = 0; 
   int position;
    
    @Override
    public void run() {
        try{
            while(true){
                if(dir == 1){
                    position++;
                    setChanged();
                    notifyObservers(position);
                    System.out.println("URCA: "+position);
                    Thread.sleep(100);
                }else if(dir == -1){
                    position--;
                    setChanged();
                    notifyObservers(position);
                    System.out.println("COBOARA: "+position);
                    Thread.sleep(100);
                }else{
                    Thread.sleep(1000); 
                    System.out.println("STATIONEAZA: "+position);
                }
                
            }
        }catch(Exception e){}
    }
    
    public void setDirection(int dir){
        this.dir = dir;
    }
    
    public int getPosition(){
        return position;
    }
    
}
