import java.util.Scanner;
//https://pastebin.com/BiHLqXys
public class ExempluSiruri {
    public static void main(String[] args){
        String s1 = "ABC";
        String s2 = "ABC";
        String s3 = null;
        String s4 = "123";
        
        //concatenari
        s1 = s1 + s2;
        System.out.println("s1="+s1);
        
        //comparare
        if(s1.equals(s2))
            System.out.println("EGAL");
        else
            System.out.println("DIFERIT");
        
        //citire linie de text de la tastatura
        Scanner input = new Scanner(System.in);
        String i1 = input.nextLine();
        int k = input.nextInt();
        System.out.println("Ati introdus:"+i1);
        
        //conversie string
        String s5 ="789";
        int d1 = Integer.parseInt(s4);
        
        //cautare substring
        String p1 = "Acesta este un test";
        int j = p1.indexOf("este");
        System.out.println("Pozitia lui 'test' "+j);
        
        //preluare caracter din tsring in array
        char c[] = p1.toCharArray();
        
    }
}
