import java.util.Random;
//https://pastebin.com/rQbVeJgQ
//https://pastebin.com/N5Cf7qWH
public class ExempluArray {
    public static void main(String[] args){
        Random r = new Random();
        int[] a = new int[10];
        
        for(int i=0;i<a.length;i++)
            System.out.println(a[i]);
        
        System.out.println("----");
        for(int i=0;i<a.length;i++)
            a[i] = r.nextInt(100);
        
        for(int x:a)
            System.out.println(x);
        
       //////////////////////////////////////////////
       
       for(int i=0;i<a.length;i++){
           for(int j=0;j<(a.length-1);j++){
               if(a[j]<a[j+1]){
                   int temp = a[j];
                   a[j] = a[j+1];
                   a[j+1] = temp;
               }
           }
       } 
       System.out.println("----");
         for(int x:a)
            System.out.println(x);
       
       boolean c = true;
       while(c){
           c = false;
           for(int j=0;j<(a.length-1);j++){
               if(a[j]<a[j+1]){
                   int temp = a[j];
                   a[j] = a[j+1];
                   a[j+1] = temp;
                   c = true;
               }
           }
       }
       
         for(int x:a)
            System.out.println(x);
        
        
    }
    
}
